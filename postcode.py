#!/usr/bin/env python

# Copyright (c) 2009 Ben Webb <bjwebb67@googlemail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# TODO Sort by date
# TODO Efficiency? Twitter direct messaging

import urllib
import time
import datetime
import twitter
import re
import sys
import random
from xml.dom import minidom

import config

dayarr = ["Sun", "Mon", "Tues", "Wed", "Thur", "Fri", "Sat"]
days = 2

def dtr(time):
    return str.lstrip(time,"0")

def remove_html_tags(data):
    p = re.compile(r'<.*?>')
    return p.sub('', data)

postcodes = []
if (len(sys.argv) == 2):
    print sys.argv[1]
    postcodes.append(sys.argv[1])
else:
    print "Incorrect number of arguments."
    exit(1)

while True:
    #random.shuffle(locations)
    for postcode in postcodes:
        dom = minidom.parse(urllib.urlopen("http://feeds.plings.net/xml.activity.php/%d/postcode/%s?APIKey=%s" % (days, postcode.replace(" ","+"), config.apikey)));
        
        doneids = []
        f = open("postcode/"+postcode)
        for line in f:
            doneids.append(line.strip())
        f.close()

        placeholder = ["No further details", "None Given"]
        minitext = []
        for node in dom.getElementsByTagName("plings"):
            for activity in node.getElementsByTagName("activity"):
                aid = activity.getAttribute("id");
                #print aid
                if aid in doneids:
                    print "Skipping"
                else:
                    doneids.append(aid)
                    starts = activity.getElementsByTagName("Starts")[0].firstChild.data
                    starts = time.strptime(starts, "%Y-%m-%d %H:%M:%S")
                    today = datetime.datetime.now()
                    startsd = datetime.datetime(*starts[0:5])
                    diff = startsd - today
                    if diff.days < days:
                        name = activity.getElementsByTagName("Name")[0].firstChild.data
                        venue = activity.getElementsByTagName("venue")[0]
                        details = activity.getElementsByTagName("Details")[0].firstChild.data
                        details = remove_html_tags(details)
                        if details in placeholder:
                            details = ""
                        vname = venue.getElementsByTagName("Name")[0].firstChild.data
                        details += " Venue: "+vname
                        nn = venue.getElementsByTagName("BuildingNameNo")[0].firstChild
                        if nn:
                            if (nn.data != vname): 
                                details += ", "+nn.data
                        street = venue.getElementsByTagName("Street")[0].firstChild
                        if street:
                            details += ", "+street.data
                        town = venue.getElementsByTagName("Town")[0].firstChild
                        if town:
                            details += ", "+town.data    
                        pcode = venue.getElementsByTagName("Postcode")[0].firstChild
                        if pcode:
                            details += ", "+pcode.data
                        ends = activity.getElementsByTagName("Ends")[0].firstChild.data
                        ends = time.strptime(ends, "%Y-%m-%d %H:%M:%S")
                        endsd = datetime.datetime(*ends[0:5])
                        times = dtr(endsd.strftime("%I:%M%p"))
                        if startsd.strftime("%p") == endsd.strftime("%p"):
                            format = "%I:%M"
                        else:
                            format = "%I:%M%p"
                        times = dtr(startsd.strftime(format))+"-"+times
                        day = startsd.strftime("%a")
                        #isgd = urllib.urlopen("http://is.gd/api.php?longurl=http://%s.placestogothingstodo.co.uk/Activity.aspx?id=%s" % (location, aid));
                        #url = isgd.readline()
                        url = ""
                        # TODO extact LA code and use that to work out the URL
                        
                        keywords = activity.getElementsByTagName("keyword")
                        tags = ""
                        i=0
                        for keyword in keywords:
                            i+=1;
                            if i > 5:
                                break;
                            k = keyword.firstChild.data
                            k = k.replace(" ","-");
                            tags = tags+"#"+k+" "
                        
                        head = "%s (%s %s): " % (name, day, times)
                        tail = " "+tags+url
                        space = 140 - len(head) - len(tail) - 2
                        main = details[:space]+".."
                        minitext.append([head+main+tail, aid]);
                    else:
                        print "no!"
                
        print "\n\n\n\n\n"
        api = twitter.Api(siteurl=config.mburl, username=postcode.replace(" ",""), password=config.password)
        api.SetUserAgent("plings2mb-postcode/0.1 +http://localhost/")
        api.SetSource("plings2mb-postcode");
        FILE = open("postcode/"+postcode,"a+")
        for mt in minitext:
            FILE.write(mt[1]+"\n")
            print len(mt[0])
            print mt[0]
            api.PostUpdates(mt[0])
        FILE.close()
    
    print "Sleep start."
    time.sleep(5*60)
    print "Sleep end."

